import {
    BeforeInsert,
    Column,
    CreateDateColumn,
    Entity, JoinTable, ManyToMany, OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import * as argon2 from "argon2";
import {argon2d} from "argon2";
import {CompanyComments} from "../../companies/entities/comments.entity";
import {classToPlain, Exclude} from "class-transformer";
import {Companies} from "../../companies/entities/companies.entity";

@Entity()
export class Users {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true, nullable: false })
    pseudo: string;

    @Column({ unique: true, nullable: false })
    email: string;

    @Column({ nullable: false })
    @Exclude({ toPlainOnly: true })
    password: string;

    @Column({ nullable: true })
    picture: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    editedAt: Date;

    @ManyToMany(() => Companies, { eager: true})
    @JoinTable()
    favorites: Companies[];

    @BeforeInsert()
    async hashPassword () {
        this.password = await argon2.hash(this.password, {type: argon2d, saltLength: 12});
    }

    toJSON() {
        return classToPlain(this);
    }

    @OneToMany(type => CompanyComments, comment => comment.user)
    comments: CompanyComments[];
}
