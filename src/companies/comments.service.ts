import { Injectable } from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Companies} from "./entities/companies.entity";
import {Like, Repository} from "typeorm";
import {CompanyComments} from "./entities/comments.entity";
import {Users} from "../users/entities/users.entity";

@Injectable()
export class CommentsService {

  constructor(
      @InjectRepository(CompanyComments) private commentsRepository: Repository<CompanyComments>,
  ) {}

  async createComment(message: string, user: Users, company: Companies): Promise<CompanyComments> {

    const comment = new CompanyComments();
    comment.message = message;
    comment.user = user;
    comment.company = company;

    return await this.commentsRepository.save(comment);
  }
}
