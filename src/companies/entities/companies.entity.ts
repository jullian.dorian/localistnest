import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import {Categories} from "../../categories/entities/categories.entity";
import {CompanyComments} from "./comments.entity";

@Entity()
export class Companies {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: false })
    name: string;

    @Column({ nullable: false })
    description: string;

    @Column({ nullable: true })
    preview: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    editedAt: Date;

    @ManyToOne(type => Categories, cat => cat.companies)
    category: Categories;

    @OneToMany(type => CompanyComments, comment => comment.company)
    comments: CompanyComments[];

    @Column({ nullable: false, type: "float"})
    latitude: number;

    @Column({ nullable: false, type: "float"})
    longitude: number;
}
