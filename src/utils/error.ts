export class ErrorBuilder {

    protected errors: ErrorProperty[];

    private constructor() {
        this.errors = [];
    }

    static make(values: ErrorProperty[]): ErrorBuilder {
        const builder = new ErrorBuilder();
        builder.errors = values;
        return builder;
    }

    static fast(values: string[]) {
        if(values) {
            if(values.length % 2 == 0) {
                const split: ErrorProperty[] = [];
                let property: ErrorProperty = {field: "", message: ""};
                for (let i = 0; i < values.length; i++) {
                    if(i % 2 == 0 || i == 0) {
                        property.field = values[i];
                    } else {
                        property.message = values[i];
                        split.push(property);
                        property = {field: "", message: ""};
                    }
                }
                return this.make(split);
            }
            throw "values should be a multiple of 2, one field and one message";
        }
        throw "values cannot be null";
    }

    toJSON(): object {
        return { errors: this.errors };
    }

    toString(): string {
        return JSON.stringify(this.toJSON());
    }

}

interface ErrorProperty {
    field: string,
    message: string
}
