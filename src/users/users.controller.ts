import {Controller, Get, Param, Post, Req, Res, UploadedFile, UseGuards, UseInterceptors} from '@nestjs/common';
import { UsersService } from './users.service';
import {FileInterceptor} from "@nestjs/platform-express";
import {diskStorage} from "multer";
import { extname } from 'path';
import {createReadStream, rmSync} from "fs";
import {Request, Response} from "express";
import {JwtAuthGuard} from "../auth/jwt.guard";
import * as fs from "fs";

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('/:id')
  async getUserId(@Param('id') id: string) {
    const user = await this.usersService.find({where: {id}});

    if(user) {
      return user;
    }
  }

  @Post('picture')
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('picture', {
    storage: diskStorage({
      destination: './files/profiles/pictures',
      filename: (req, file, cb) => {
        console.log("file name process");
        const d = Date.now();
        cb(null,`${d}${extname(file.originalname)||".jpg"}`);
      }
    })
  }))
  async changeProfilePicture(@UploadedFile() picture: Express.Multer.File, @Req() req: Request) {
    console.log("upload file");
    const user = req.user;
    console.log("upload by user: ", user);
    const users = await this.usersService.findIdEmail(user);
    console.log("find user by mail", users);

    if(users) {
      const pict = users.picture;
      console.log("change user picture");
      await this.usersService.changePicture(users, picture.filename);
      if(pict) {
        try {
          rmSync("./files/profiles/pictures/" + pict);
          console.log("Deleted old picture: " + pict);
        } catch (e) {
          console.log(e);
        }
      }
    }

    return picture.filename;
  }

  @Get('picture/:path')
  async getProfilePicture(@Param('path') path: string,  @Res() response: Response) {
    console.log("get file path");
    try {
      if(fs.existsSync("./files/profiles/pictures/" + path)) {
        const file = createReadStream("./files/profiles/pictures/" + path);
        file.pipe(response);
      } else {
        return "Path not exist";
      }
    } catch (e) {
      console.log(e);
    }
  }
}
