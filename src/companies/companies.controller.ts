import {Body, Controller, Get, Param, Post, Req, Res, UseGuards} from '@nestjs/common';
import {CompaniesService} from './companies.service';
import {JwtAuthGuard} from "../auth/jwt.guard";
import * as fs from "fs";
import {createReadStream} from "fs";
import {Request, Response} from "express";
import {CommentsService} from "./comments.service";
import {UsersService} from "../users/users.service";

@Controller('companies')
export class CompaniesController {
  constructor(private readonly companiesService: CompaniesService,
              private readonly commentsService: CommentsService,
              private readonly usersService: UsersService) {}

  @Get()
  findAll() {
    return this.companiesService.findAll();
  }

  @Get('category/:id')
  @UseGuards(JwtAuthGuard)
  findByCategory(@Param('id') id: string) {
    return this.companiesService.findByCategory(+id);
  }

  @Get('category/:cat/:name')
  @UseGuards(JwtAuthGuard)
  async findByCategoryAndName(@Param('cat') id: string, @Param('name') name: string) {
    return await this.companiesService.findByCategoryName(id, name);
  }

  @Get('/:id/comments')
  @UseGuards(JwtAuthGuard)
  async findComments(@Param('id') id: string) {
    return await this.companiesService.findComments(id);
  }

  @Post('/:id/comments')
  @UseGuards(JwtAuthGuard)
  async postComment(@Param('id') id: string, @Body() body: any, @Req() req: Request) {
    const { message } = body;
    const u = req.user;
    const user = await this.usersService.findIdEmail(u);
    if(user) {
      const company = await this.companiesService.findById(id);
      if(company) {
        return await this.commentsService.createComment(message, user, company);
      }
    }
  }

  @Get('/:id/favorite')
  @UseGuards(JwtAuthGuard)
  async getFavorite(@Param('id') id: string, @Req() req: Request) {
    const u = req.user;
    const user = await this.usersService.findIdEmail(u);
    if(user) {
      const company = await this.companiesService.findById(id);
      if (company) {
        const favorite = user.favorites.find((c) => c.id == company.id) != undefined;
        return {isFavorite: favorite};
      }
    }
    return { isFavorite: false };
  }

  @Post('/:id/favorite')
  @UseGuards(JwtAuthGuard)
  async changeFavorite(@Param('id') id: string, @Body() body: { favorite: boolean }, @Req() req: Request) {
    const u = req.user;
    const user = await this.usersService.findIdEmail(u);
    if(user) {
      const company = await this.companiesService.findById(id);
      if (company) {
        return {isFavorite: true};
      }
    }
    return { isFavorite: false };
  }

  @Get('/:id/picture')
  async getCompanyPicture(@Param('id') id: string, @Res() response: Response) {
    const company = await this.companiesService.findById(id);
    console.log(company);
    if(company) {
      console.log("ok find")
      try {
        const dir = "./files/companies/pictures/" + company.preview;
        if(fs.existsSync(dir)) {
          console.log("return file");
          const file = createReadStream(dir);
          file.pipe(response);
        } else {
          return "Path not exist";
        }
      } catch (e) {
        console.log(e);
      }
    }
  }
}
