import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {AuthModule} from './auth/auth.module';
import {ConfigModule} from "@nestjs/config";
import {TypeOrmModule} from "@nestjs/typeorm";
import {UsersModule} from './users/users.module';
import {CompaniesModule} from './companies/companies.module';
import {Users} from "./users/entities/users.entity";
import {CategoriesModule} from './categories/categories.module';
import {Companies} from "./companies/entities/companies.entity";
import {Categories} from "./categories/entities/categories.entity";

@Module({
    imports: [ConfigModule.forRoot(),
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: 'localhost',
            database: "localise",
            port: 3306,
            username: 'root',
            password: '',
            entities: [Users, Companies, Categories],
            synchronize: true,
            autoLoadEntities: true
        }),
        AuthModule,
        UsersModule,
        CompaniesModule,
        CategoriesModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
