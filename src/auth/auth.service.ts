import { Injectable } from '@nestjs/common';
import {UsersService} from "../users/users.service";
import {JwtService} from "@nestjs/jwt";
import * as argon2 from "argon2";
import {AuthLoginDto} from "./dto/login-auth.dto";

@Injectable()
export class AuthService {

  constructor(private usersService: UsersService,
              private jwtService: JwtService) {}

  /**
   * On renvoit l'utilisateur sans le mot de passe
   * @param login
   */
  async validateUser(login: AuthLoginDto): Promise<any> {
    const user = await this.usersService.findOne(login.email);
    if (user && await argon2.verify(user.password, login.password)) {
      const token = await this.generateToken(user.id, user.email);
      const { password, ...result } = user;
      return { token, user: result};
    }
    return null;
  }

  async generateToken(userId: number, email: string) {
    return await this.jwtService.signAsync({ sub: userId, email })
  }
}
