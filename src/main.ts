import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {CustomValidationPipe} from "./utils/validation";
import {useContainer} from "typeorm";
import 'reflect-metadata';
import {Container} from "typedi";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  console.log("starting app on [::1]:3000");
  useContainer(app.select(AppModule), { fallbackOnErrors: true });
  useContainer(Container);
  app.useGlobalPipes(new CustomValidationPipe());
  app.setGlobalPrefix('api')
  await app.listen(3000);
}
bootstrap();
