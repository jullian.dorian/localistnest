import {IsDefined, IsEmail, IsString, Length, Matches} from "class-validator";
import {
    IsUserEmailNotExist,
    IsUserPseudoNotExist,
} from "../../utils/validation";

export class AuthRegisterDto {

    @IsDefined({
        message: "Le champ pseudo ne peut pas être vide."
    })
    @IsString({
        message: "Le pseudo doit être une chaîne de caractère."
    })
    @Matches(/^[a-zA-Z0-9]+[-_^]{0,3}$/, {
        message: "Merci de respecter le format: lettres, chiffres et 3 tirets maximum."
    })
    @Length(4, 24, {
        message: "Le pseudo doit faire entre 4 et 24 caractères."
    })
    @IsUserPseudoNotExist()
    readonly pseudo: string;

    @IsDefined({
        message: "Le champ email ne peut pas être vide."
    })
    @IsEmail({}, {
        message: "Le champ doit être au format email."
    })
    @IsUserEmailNotExist()
    readonly email: string;


    @IsDefined({
        message: "Le champ mot de passe ne peut pas être vide."
    })
    @IsString({
        message: "Le mot de passe doit être une chaîne de caractère."
    })
    @Length(6, 32, {
        message: "Le mot de passe doit faire entre 6 et 32 caractères."
    })
    readonly password: string;
}
