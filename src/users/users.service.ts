import { Injectable } from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Users} from "./entities/users.entity";
import {FindOneOptions, Repository} from "typeorm";
import {UserInfo} from "os";

@Injectable()
export class UsersService {

  constructor(
      @InjectRepository(Users)
      private usersRepository: Repository<Users>,
  ) {}

  async create(createUserDto: any): Promise<Users> {
    const user = new Users();
    user.pseudo = createUserDto.pseudo;
    user.password = createUserDto.password;
    user.email = createUserDto.email;

    return await this.usersRepository.save(user);
  }

  async findAll() {
    return this.usersRepository.find();
  }

  async findOne(email: string) {
    return await this.usersRepository.findOne({ where: { email: email}});
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }

  async find(filter?: FindOneOptions<Users>): Promise<Users> {
      return await this.usersRepository.findOne(filter);
  }

  async findIdEmail({id, email}: any) {
    return await this.usersRepository.findOne({where: [{id}, {email}]});
  }

  async changePicture(user: Users, filename: string) {
    user.picture = filename;
    await this.usersRepository.save(user);
  }
}
