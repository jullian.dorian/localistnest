import {Controller, Get, Post, Body, BadRequestException, UseGuards, Request, UsePipes} from '@nestjs/common';
import { AuthService } from './auth.service';
import {UsersService} from "../users/users.service";
import {AuthLoginDto} from "./dto/login-auth.dto";
import {AuthRegisterDto} from "./dto/register-auth.dto";
import {JwtAuthGuard} from "./jwt.guard";
import {CustomValidationPipe} from "../utils/validation";
import {ErrorBuilder} from "../utils/error";

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService,
              private readonly usersService: UsersService) {}

  @Post('login')
  async login(@Body() loginDto: AuthLoginDto) {

    console.log("login route");

    //On a un utilisateur selon les données de connexion
    const user = await this.authService.validateUser(loginDto);

    //Si le user est null c'est que le mot de passe ne correspond pas.
    if(!user) {
      throw new BadRequestException(ErrorBuilder.fast(["password", "Le mot de passe ne correspond pas à l'email associé."]));
    }

    return user;
  }

  @Post('register')
  async register(@Body() registerDto: AuthRegisterDto) {

    console.log("register route");

    const user = await this.usersService.create(registerDto);

    //Creation du token
    const token = await this.authService.generateToken(user.id, user.email);

    const { password, ...infos } = user;

    console.log(user);

    return { token, user: infos};
  }

  @Get('ping')
  @UseGuards(JwtAuthGuard)
  async ping() {
    console.log("je te ping fdp")
    return "ok";
  }

  @Post('forgot')
  async forgot() {
    return null;
  }

  @Post('forgot/:token')
  async forgotVerify() {

  }

  @UseGuards(JwtAuthGuard)
  @Get('/test')
  test(@Request() req) {
    return req.user;
  }
}
