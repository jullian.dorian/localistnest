import {Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Req, Query, Res} from '@nestjs/common';
import { CategoriesService } from './categories.service';
import {JwtAuthGuard} from "../auth/jwt.guard";
import {Response} from "express";
import * as fs from "fs";
import {createReadStream} from "fs";

@Controller('categories')
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {}

  @Get()
  async findAll() {
    return await this.categoriesService.findAll();
  }


  @Get('/search/:name')
  @UseGuards(JwtAuthGuard)
  async findName(@Param('name') name: string) {
    console.log("im on search name")
    return await this.categoriesService.findName(name);
  }

  @Get('/search')
  @UseGuards(JwtAuthGuard)
  findAllSearch(@Param() params) {
    console.log("im on all")
    return this.findAll();
  }

  @Get(':id/picture')
  async getPicture(@Param('id') id: string, @Res() response: Response) {
    const category = await this.categoriesService.findOne(id);
    console.log(category);
    if(category) {
      console.log("ok find")
      try {
        const dir = "./files/categories/pictures/" + category.picture;
        if(fs.existsSync(dir)) {
          console.log("return file");
          const file = createReadStream(dir);
          file.pipe(response);
        } else {
          return "Path not exist";
        }
      } catch (e) {
        console.log(e);
      }
    }
  }

/*
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCategoryDto: UpdateCategoryDto) {
    return this.categoriesService.update(+id, updateCategoryDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.categoriesService.remove(+id);
  }*/
}
