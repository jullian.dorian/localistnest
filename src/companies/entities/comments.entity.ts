import {Column, CreateDateColumn, Entity, JoinColumn, JoinTable, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Users} from "../../users/entities/users.entity";
import {Companies} from "./companies.entity";

@Entity()
export class CompanyComments {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Users, {
        onDelete: "CASCADE",
        nullable: false,
    })
    user: Users;

    @ManyToOne(type => Companies, {
        onDelete: "CASCADE",
        nullable: false,
    })
    company: Companies;

    @Column()
    message: string;

    @CreateDateColumn()
    createdAt: Date;

}
