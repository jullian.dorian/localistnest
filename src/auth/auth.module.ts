import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import {UsersModule} from "../users/users.module";
import {PassportModule} from "@nestjs/passport";
import {JwtModule} from "@nestjs/jwt";
import {jwtSecretKey} from "./constants";
import {JwtStrategy} from "./jwt.strategy";
import {UsersService} from "../users/users.service";
import {
    IsUserEmailExistConstraint,
    IsUserEmailNotExistConstraint,
    IsUserPseudoNotExistConstraint
} from "../utils/validation";

@Module({
  imports: [
      UsersModule,
      PassportModule,
      JwtModule.register({
        secret: jwtSecretKey,
        signOptions: { expiresIn: '2h' },
      }),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, UsersService, IsUserEmailNotExistConstraint, IsUserEmailExistConstraint, IsUserPseudoNotExistConstraint],
  exports: [AuthService, JwtModule]
})
export class AuthModule {}
