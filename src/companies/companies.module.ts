import { Module } from '@nestjs/common';
import { CompaniesService } from './companies.service';
import { CompaniesController } from './companies.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Companies} from "./entities/companies.entity";
import {CompanyComments} from "./entities/comments.entity";
import {UsersService} from "../users/users.service";
import {CommentsService} from "./comments.service";
import {UsersModule} from "../users/users.module";

@Module({
  imports: [TypeOrmModule.forFeature([Companies, CompanyComments]), UsersModule],
  controllers: [CompaniesController],
  providers: [CompaniesService, CommentsService],
})
export class CompaniesModule {}
