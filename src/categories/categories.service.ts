import {Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Like, Repository} from "typeorm";
import {Categories} from "./entities/categories.entity";

@Injectable()
export class CategoriesService {

    constructor(@InjectRepository(Categories) private readonly repository: Repository<Categories>) {}

    async findAll() {
        return await this.repository.find();
    }

    async findOne(id: number|string) {
      return await this.repository.findOne(id);
    }

    async findName(name: string) {
        console.log(name);
        return await this.repository.find({ where: { name: Like("%"+name+"%")}})
    }
}
