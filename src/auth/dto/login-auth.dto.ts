import {IsDefined, IsEmail, IsString, Length, Validate} from "class-validator";
import {IsUserEmailExist} from "../../utils/validation";

export class AuthLoginDto {

    @IsDefined({
        message: "Le champ email ne peut pas être vide."
    })
    @IsEmail({}, {
        message: "Le champ doit être au format email."
    })
    @IsUserEmailExist()
    readonly email: string;

    @IsDefined({
        message: "Le champ mot de passe ne peut pas être vide."
    })
    @IsString({
        message: "Le mot de passe doit être une chaîne de caractère."
    })
    @Length(6, 32, {
        message: "Le mot de passe doit faire entre 6 et 32 caractères."
    })
    readonly password: string;
}
