import {
    ArgumentMetadata,
    BadRequestException,
    HttpException,
    HttpStatus,
    Injectable,
    PipeTransform
} from "@nestjs/common";
import {
    registerDecorator,
    validate,
    ValidationArguments,
    ValidationOptions,
    ValidatorConstraint,
    ValidatorConstraintInterface
} from "class-validator";
import {plainToClass} from "class-transformer";
import {Repository} from "typeorm";
import {Users} from "../users/entities/users.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {UsersService} from "../users/users.service";

@Injectable()
export class CustomValidationPipe implements PipeTransform<any, any> {
    async transform(value: any, metadata: ArgumentMetadata): Promise<any> {
        if (!value) {
            throw new BadRequestException('No data submitted');
        }

        const { metatype } = metadata;
        if (!metatype || !this.toValidate(metatype)) {
            return value;
        }
        const object = plainToClass(metatype, value);
        const errors = await validate(object);
        if (errors.length > 0) {
            throw new HttpException({errors:  this.buildError(errors)}, HttpStatus.BAD_REQUEST);
        }
        return value;
    }

    private buildError(errors) {
        //creation du tableau
        const result = [];
        errors.forEach(el => {
            for (let constraint of Object.entries(el.constraints)) {
                let component = {};
                component['field'] = el.property;
                component['message'] = constraint[1];
                result.push(component);
                break;
            }
        });
        return result;
    }

    private toValidate(metatype): boolean {
        const types = [String, Boolean, Number, Array, Object];
        return !types.find((type) => metatype === type);
    }

    /**
     * Attente de la pipe:
     * {
     *     errors: [
     *         {
     *             "field": string,
     *             "message": string,
     *         }
     *     ]
     * }
     */
}

@ValidatorConstraint({ name: "UserEmailExist", async: true })
@Injectable()
export class IsUserEmailExistConstraint implements ValidatorConstraintInterface {

    static repository: Repository<Users> = null;

    constructor(@InjectRepository(Users) private repository: Repository<Users>) {
        if(repository) {
            IsUserEmailExistConstraint.repository = repository;
        }
    }

    async validate(email: string, args: ValidationArguments): Promise<boolean> {
        if(IsUserEmailExistConstraint.repository) {
            try {
                return await IsUserEmailExistConstraint.repository.findOne({ where : { email }}).then(user => {
                    return user != null;
                });
            } catch (e) {
                return false;
            }
        }
        return false;
    }

    defaultMessage(validationArguments?: ValidationArguments): string {
        return "Cet email n'existe pas";
    }
}

export function IsUserEmailExist(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            name: "isUserEmailExist",
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            async: true,
            constraints: [object],
            validator: IsUserEmailExistConstraint,
        });
    };
}


@ValidatorConstraint({ name: "UserEmailExist", async: true })
@Injectable()
export class IsUserEmailNotExistConstraint implements ValidatorConstraintInterface {

    static repository: Repository<Users> = null;

    constructor(@InjectRepository(Users) private repository: Repository<Users>) {
        if(repository) {
            IsUserEmailNotExistConstraint.repository = repository;
        }
    }

    async validate(email: string, args: ValidationArguments): Promise<boolean> {
        if(IsUserEmailNotExistConstraint.repository) {
            try {
                return await IsUserEmailNotExistConstraint.repository.findOne({ where : { email }}).then(user => {
                    console.log(user);
                    return user == null;
                });
            } catch (e) {
                return false;
            }
        }
        return false;
    }

    defaultMessage(validationArguments?: ValidationArguments): string {
        return "Cet email est déjà utilisé";
    }
}

export function IsUserEmailNotExist(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            name: "isUserEmailNotExist",
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            async: true,
            constraints: [object],
            validator: IsUserEmailNotExistConstraint,
        });
    };
}

@ValidatorConstraint({ name: "UserPseudoAlreadyExist", async: true })
@Injectable()
export class IsUserPseudoNotExistConstraint implements ValidatorConstraintInterface {

    static repository: Repository<Users> = null;

    constructor(@InjectRepository(Users) private repository: Repository<Users>) {
        if(repository) {
            IsUserPseudoNotExistConstraint.repository = repository;
        }
    }

    async validate(pseudo: string, args: ValidationArguments): Promise<boolean> {
        if(IsUserPseudoNotExistConstraint.repository) {
            try {
                return await IsUserPseudoNotExistConstraint.repository.findOne({ where: { pseudo }}).then(user => {
                    return user == null;
                });
            } catch (e) {
                return false;
            }
        }

        return false;
    }

    defaultMessage(validationArguments?: ValidationArguments): string {
        return "Ce pseudo est déjà utilisé.";
    }
}

export function IsUserPseudoNotExist(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            name: "isUserPseudoExist",
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            async: true,
            constraints: [object],
            validator: IsUserPseudoNotExistConstraint,
        });
    };
}
