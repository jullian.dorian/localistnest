import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Companies} from "../../companies/entities/companies.entity";

@Entity()
export class Categories {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true, nullable: false })
    name: string;

    @Column({ nullable: true })
    picture: string;

    @OneToMany(type => Companies, company => company.category)
    companies: Companies[];

}
