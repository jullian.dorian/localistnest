import { Injectable } from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Companies} from "./entities/companies.entity";
import {Like, Repository} from "typeorm";
import {CompanyComments} from "./entities/comments.entity";

@Injectable()
export class CompaniesService {

  constructor(
      @InjectRepository(Companies) private companiesRepository: Repository<Companies>,
      @InjectRepository(CompanyComments) private commentsRepository: Repository<CompanyComments>,
  ) {}

  async findAll() {
    return await this.companiesRepository.find();
  }

  async findById(id: string): Promise<Companies> {
    return await this.companiesRepository.findOne({ where : { id }});
  }

  async findByCategory(idCategory: number) {
      return await this.companiesRepository.find({ where: { category : { id : idCategory }}})
  }

  async findByCategoryName(id: string, name: string) {
    return await this.companiesRepository.find({ where: { category: { id: id }, name: Like('%'+name+'%')}})
  }

  async findComments(id: string) {
    return await this.commentsRepository.createQueryBuilder("comments")
        .leftJoin("comments.user", "user")
        .addSelect(["user.id", "user.pseudo", "user.picture"])
        .where("comments.company = :id", { id: id})
        .getMany()
  }
}
